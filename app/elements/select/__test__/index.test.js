import React from 'react';
import {mount} from 'enzyme';
import Select from '../index';

describe('Select Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Select/>
    );

    expect(component.find('div').first().hasClass('ant-select')).toEqual(true);
  });
});
