import React from 'react';
import PropTypes from 'prop-types';
import AntSelect from 'antd/lib/select';

const Option = AntSelect.Option;

/**
 * elements/select : Select elements
 * @param {object} props the properties of the element
 * @returns {HTMLElement} returns select element's node
 **/
class Select extends React.Component {
  render() {
    return (
      <AntSelect {...this.props}>
        {this.props.options.map(option => (
          <Option
            value={option.value || option}
            key={option.value || option}
          >
            {option.label || option}
          </Option>
        ))}
      </AntSelect>
    );
  }
}

Select.defaultProps = {
  options: []
};

Select.propTypes = {
  options: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    label: PropTypes.string
  }), PropTypes.string]))
};

export default Select;
