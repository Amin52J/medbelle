import React from 'react';
import PropTypes from 'prop-types';

/**
 * elements/pin : Pin element
 * @param {object} props the properties of the component
 * @param {string} props.color the color of the pin
 * @param {number} props.lat the latitude of the pin
 * @param {number} props.lng the longitude of the pin
 * @param {function} props.onClick the click event callback
 * @returns {HTMLElement} returns pin element's node
 **/
const Pin = props => (
  <div
    className={`pin-element ${props.className || ''}`}
    style={{backgroundColor: props.color}}
    onClick={props.onClick}
  >
    {props.children}
  </div>
);

Pin.defaultProps = {
  onClick: () => {
  }
};

Pin.propTypes = {
  color: PropTypes.string,
  lat: PropTypes.number,
  lng: PropTypes.number,
  onClick: PropTypes.func
};

export default Pin;
