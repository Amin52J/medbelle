import React from 'react';
import {mount} from 'enzyme';
import Pin from '../index';

describe('Pin Element', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Pin/>
    );

    expect(component.find('div').first().hasClass('pin-element')).toEqual(true);
  });
});
