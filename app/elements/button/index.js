import React from 'react';
import PropTypes from 'prop-types';
import AntButton from 'antd/lib/button';

/**
 * elements/button : Button elements
 * @param {object} props the properties of the element
 * @param {boolean} props.fullWidth whether the button must be full width or not
 * @returns {HTMLElement} returns button element's node
 **/
const Button = props => (
  <AntButton
    {...props}
    className={`${props.fullWidth ? 'ant-full-width' : ''} ${props.type === 'secondary' ? 'ant-btn-secondary' : ''} ${props.className || ''}`}
  >
    {props.children}
  </AntButton>
);

Button.defaultProps = {};

Button.propTypes = {
  fullWidth: PropTypes.bool
};

export default Button;
