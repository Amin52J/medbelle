import React from 'react';
import {mount} from 'enzyme';
import Button from '../index';

describe('Button Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Button/>
    );

    expect(component.find('button').first().hasClass('ant-btn')).toEqual(true);
  });
});
