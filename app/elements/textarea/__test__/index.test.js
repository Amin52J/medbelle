import React from 'react';
import {mount} from 'enzyme';
import Textarea from '../index';

describe('Textarea Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Textarea/>
    );

    expect(component.find('textarea').first().hasClass('ant-input')).toEqual(true);
  });
});
