import React from 'react';
import AntInput from 'antd/lib/input';

const AntTextArea = AntInput.TextArea;

/**
 * elements/input : TextArea elements
 * @param {object} props the properties of the element
 * @returns {HTMLElement} returns input element's node
 **/
class TextArea extends React.Component {
  render() {
    return (
      <AntTextArea {...this.props} rows={4}/>
    );
  }
}

TextArea.defaultProps = {};

TextArea.propTypes = {};

export default TextArea;
