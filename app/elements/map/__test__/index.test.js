import React from 'react';
import {mount} from 'enzyme';
import Map from '../index';

describe('Map Element', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Map/>
    );

    expect(component.find('div').first().hasClass('map-element')).toEqual(true);
  });
});
