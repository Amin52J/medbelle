import React from 'react';
import Common from '@constants/common';
import GoogleMapReact from 'google-map-react';
import PropTypes from 'prop-types';
import Pin from '@elements/pin';

/**
 * elements/map : Map element
 * @param {object} props the properties of the component
 * @returns {HTMLElement} returns map element's node
 **/
class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addPoint: {
        lat: 0,
        lng: 0
      }
    };

    this.showAddPoint = this.showAddPoint.bind(this);
  }

  showAddPoint(e) {
    this.setState({
      addPoint: {
        lat: e.lat,
        lng: e.lng
      }
    });
  }

  render() {
    return (
      <div className="map-element" id="map">
        {
          <GoogleMapReact
            defaultCenter={Common.values.map.center}
            defaultZoom={Common.values.map.zoom}
            bootstrapURLKeys={{
              key: Common.values.map.key
            }}
            onChange={this.props.onChange}
            onClick={this.showAddPoint}
          >
            {this.props.children}
            {
              this.state.addPoint.lat !== 0 &&
              this.state.addPoint.lng !== 0 &&
              <Pin
                onClick={() => {
                  this.props.onAddPointClick(this.state.addPoint);
                }}
                lat={this.state.addPoint.lat}
                lng={this.state.addPoint.lng}
                className="add-point"
              >
                +
              </Pin> || null
            }
          </GoogleMapReact>
        }
        <div className="overlay"/>
      </div>
    );
  }
}

Map.defaultProps = {};

Map.propTypes = {
  onChange: PropTypes.func,
  onAddPointClick: PropTypes.func
};

export default Map;
