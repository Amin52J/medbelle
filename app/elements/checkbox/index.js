import React from 'react';
import AntCheckbox from 'antd/lib/checkbox';

/**
 * elements/checkbox : Checkbox elements
 * @param {object} props the properties of the element
 * @returns {HTMLElement} returns checkbox element's node
 **/
const Checkbox = props => (
  <AntCheckbox
    {...props}
    className={props.className}
  >
    {props.children}
  </AntCheckbox>
);

Checkbox.defaultProps = {};

Checkbox.propTypes = {};

export default Checkbox;
