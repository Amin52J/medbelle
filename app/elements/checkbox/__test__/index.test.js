import React from 'react';
import {mount} from 'enzyme';
import Checkbox from '../index';

describe('Checkbox Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Checkbox/>
    );

    expect(component.find('label').first().hasClass('ant-checkbox-wrapper')).toEqual(true);
  });
});
