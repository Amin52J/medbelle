import React from 'react';
import AntDatePicker from 'antd/lib/date-picker';

/**
 * elements/datePicker : DatePicker elements
 * @param {object} props the properties of the element
 * @returns {HTMLElement} returns datePicker element's node
 **/
const DatePicker = props => (
  <AntDatePicker
    {...props}
    className={props.className}
  >
    {props.children}
  </AntDatePicker>
);

DatePicker.defaultProps = {};

DatePicker.propTypes = {};

export default DatePicker;
