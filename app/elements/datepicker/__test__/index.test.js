import React from 'react';
import {mount} from 'enzyme';
import DatePicker from '../index';

describe('DatePicker Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <DatePicker/>
    );

    expect(component.find('span').first().hasClass('ant-calendar-picker')).toEqual(true);
  });
});
