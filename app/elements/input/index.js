import React from 'react';
import AntInput from 'antd/lib/input';

/**
 * elements/input : Input elements
 * @param {object} props the properties of the element
 * @returns {HTMLElement} returns input element's node
 **/
class Input extends React.Component {
  render() {
    return (
      <AntInput {...this.props}/>
    );
  }
}

Input.defaultProps = {};

Input.propTypes = {};

export default Input;
