import React from 'react';
import {mount} from 'enzyme';
import Input from '../index';

describe('Input Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Input/>
    );

    expect(component.find('input').first().hasClass('ant-input')).toEqual(true);
  });
});
