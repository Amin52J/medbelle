import React from 'react';
import {mount} from 'enzyme';
import Form from '../index';

describe('Form Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Form/>
    );

    expect(component.find('form').first().hasClass('form-element')).toEqual(true);
  });
});
