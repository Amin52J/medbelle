import React from 'react';

/**
 * elements/form : Form elements
 * @param {Object} props the properties of the form
 * @returns {HTMLElement} returns form element's node
 **/
class Form extends React.Component {
  render() {
    return (
      <form className="form-element" {...this.props}/>
    );
  }
}

Form.defaultProps = {};

Form.propTypes = {};

export default Form;
