import * as types from '@constants/actionTypes';

/**
 * containers/authentication/action.js : LOGIN action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the login action object
 **/
export function login(payload) {
  return {
    type: types.LOGIN,
    payload
  };
}

/**
 * containers/authentication/action.js : LOGIN_SUCCESS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the loginSuccess action object
 **/
export function loginSuccess(payload) {
  return {
    type: types.LOGIN_SUCCESS,
    payload
  };
}

/**
 * containers/authentication/action.js : LOGIN_ERROR action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the loginError action object
 **/
export function loginError(payload) {
  return {
    type: types.LOGIN_ERROR,
    payload
  };
}

/**
 * containers/authentication/action.js : LOGIN_CANCEL action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the loginCancel action object
 **/
export function loginCancel(payload) {
  return {
    type: types.LOGIN_CANCEL,
    payload
  };
}
