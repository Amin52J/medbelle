import * as types from '@constants/actionTypes';
import * as actions from '@containers/authentication/action';
import Common from '@constants/common';

/**
 * containers/authentication/epic.js : login epic
 * @param {Object} action$ stream of actions
 * @param {Object} store the main store to be used
 * @param {Object} dependencies the dependencies to be injected inside the epic
 *   @param {Function} dependencies.ajax the custom rxjs ajax object
 *   @param {Function} dependencies.of the rxjs of function
 * @returns {Array} an array of actions to be dispatched
 **/
export const login = (action$, store, {ajax, of}) => action$.ofType(types.LOGIN)
  .mergeMap(action => ajax.post(`${Common.api.base}/${Common.config.APP_ID}/${Common.config.API_KEY}${Common.api.login}`, {...action.payload})
    .map(response => actions.loginSuccess(response))
    .takeUntil(action$.ofType(types.LOGIN_CANCEL))
    .catch(error => of(actions.loginError(error)))
  );
