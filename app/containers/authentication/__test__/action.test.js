import React from 'react';
import * as actions from '../action';
import * as types from '../../../constants/actionTypes';

describe('Authentication Container - actions', () => {
  it('dispatches login action', () => {
    expect(actions.login('some text')).toEqual({
      type: types.LOGIN,
      payload: 'some text'
    });
  });

  it('dispatches loginSuccess action', () => {
    expect(actions.loginSuccess('some text')).toEqual({
      type: types.LOGIN_SUCCESS,
      payload: 'some text'
    });
  });

  it('dispatches loginError action', () => {
    expect(actions.loginError('some text')).toEqual({
      type: types.LOGIN_ERROR,
      payload: 'some text'
    });
  });

  it('dispatches loginCancel action', () => {
    expect(actions.loginCancel('some text')).toEqual({
      type: types.LOGIN_CANCEL,
      payload: 'some text'
    });
  });
});
