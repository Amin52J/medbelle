import React from 'react';
import {mount} from 'enzyme';
import Authentication from '../index';
import store, {history} from '../../../constants/store';
import {ConnectedRouter} from 'react-router-redux';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

describe('Authentication Container - Container', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Authentication/>
        </ConnectedRouter>
      </Provider>
    );
    const tree = renderer.create(component).toJSON();

    expect(component.find('div').first().hasClass('authentication-page')).toEqual(true);
    expect(tree).toMatchSnapshot();
  });
});
