import React from 'react';
import * as epics from '../epic';
import * as types from '../../../constants/actionTypes';
import {ActionsObservable} from 'redux-observable';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/takeUntil';

const action$ = ActionsObservable.of({type: types.LOGIN, payload: '/api/login'});

describe('Authentication Container - epics', () => {
  it('successfully gets login action result', done => {
    const ajax = {
      post: () => Observable.of({})
    };
    const expectedOutputActions = [{type: types.LOGIN_SUCCESS, payload: {}}];
    epics.login(action$, null, {ajax})
      .toArray()
      .subscribe(actualOutputActions => {
          expect(actualOutputActions).toEqual(expectedOutputActions);
          done();
        }
      );
  });

  it('fails to get login action result', done => {
    const ajax = {
      post: () => Observable.throw('failed')
    };
    const expectedOutputActions = [{type: types.LOGIN_ERROR, payload: 'failed'}];
    epics.login(action$, null, {ajax, of: Observable.of})
      .toArray()
      .subscribe(actualOutputActions => {
          expect(actualOutputActions).toEqual(expectedOutputActions);
          done();
        }
      );
  });
});
