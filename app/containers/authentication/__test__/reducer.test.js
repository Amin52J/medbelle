import React from 'react';
import reducer from '../reducer';
import * as types from '../../../constants/actionTypes';

describe('Authentication Container - reducers', () => {
  it('sets loading state', () => {
    expect(reducer({}, {
      type: types.LOGIN
    })).toEqual({
      loading: true
    });
  });

  it('sets success state', () => {
    expect(reducer({}, {
      type: types.LOGIN_SUCCESS
    })).toEqual({
      loading: false
    });
  });

  it('sets error state', () => {
    expect(reducer({}, {
      type: types.LOGIN_ERROR
    })).toEqual({
      loading: false
    });
  });

  it('sets cancel state', () => {
    expect(reducer({}, {
      type: types.LOGIN_CANCEL
    })).toEqual({
      loading: false
    });
  });

  it('returns the initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });
});
