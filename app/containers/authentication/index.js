import React from 'react';
import * as actions from '@containers/authentication/action';
import Common from '@constants/common';

/* hoc */
import reduxConnect from '@hoc/reduxConnect';

/* elements */
import Input from '@elements/input';
import Button from '@elements/button';
import Form from '@elements/form';

/**
 * containers/authentication : Authentication container
 * @param {Object} props the properties of the container
 * @param {Object} props.authentication the redux state of the container
 * @param {Object} props.authentication.test the test property of the container
 **/
class Authentication extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(e) {
    e.preventDefault();
    this.props.login({
      login: this.state.email,
      password: this.state.password
    });
  }

  handleInputChange(e) {
    const newState = {...this.state};
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  }

  render() {
    const {authenticationState: {loading}} = this.props;

    return (
      <div className="authentication-page">
        <Form onSubmit={this.handleLogin}>
          <h1>{Common.texts.medbelle}</h1>
          <h3>{Common.texts.login}</h3>
          <Input name="email" placeholder={Common.texts.email} type="email" onChange={this.handleInputChange}/>
          <Input name="password" placeholder={Common.texts.password} type="password" onChange={this.handleInputChange}/>
          <Button type="primary" htmlType="submit" disabled={loading}>{Common.texts.login}</Button>
        </Form>
      </div>
    );
  }
}

export default reduxConnect(Authentication, actions);
