import * as types from '@constants/actionTypes';
import Cookies from 'js-cookie';
import Common from '@constants/common';

const initialState = {};

/**
 * containers/authentication/reducer.js : authentication reducer
 * @param {Object} state the state of the authentication container
 * @param {Object} action the redux action instance
 * @returns {Object} returns the new state
 **/
export default function authenticationState(state = initialState, action) {
  switch (action.type) {
    case types.LOGIN:
      Cookies.remove(Common.cookie.user);

      return {
        ...state,
        loading: true
      };

    case types.LOGIN_SUCCESS:
      if (action.payload && action.payload.response) {
        Cookies.set(Common.cookie.user, {...action.payload.response});
      }

      return {
        ...state,
        loading: false
      };

    case types.LOGIN_ERROR:
      return {
        ...state,
        loading: false
      };

    case types.LOGIN_CANCEL:
      return {
        ...state,
        loading: false
      };

    default:
      return state;
  }
}
