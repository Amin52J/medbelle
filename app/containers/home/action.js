import * as types from '@constants/actionTypes';

/**
 * containers/home/action.js : GET_CATEGORIES action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getCategories action object
 **/
export function getCategories(payload) {
  return {
    type: types.GET_CATEGORIES,
    payload
  };
}

/**
 * containers/home/action.js : GET_CATEGORIES_SUCCESS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getCategoriesSuccess action object
 **/
export function getCategoriesSuccess(payload) {
  return {
    type: types.GET_CATEGORIES_SUCCESS,
    payload
  };
}

/**
 * containers/home/action.js : GET_CATEGORIES_ERROR action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getCategoriesError action object
 **/
export function getCategoriesError(payload) {
  return {
    type: types.GET_CATEGORIES_ERROR,
    payload
  };
}

/**
 * containers/home/action.js : GET_CATEGORIES_CANCEL action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getCategoriesCancel action object
 **/
export function getCategoriesCancel(payload) {
  return {
    type: types.GET_CATEGORIES_CANCEL,
    payload
  };
}

/**
 * containers/home/action.js : ADD_CATEGORY action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addCategory action object
 **/
export function addCategory(payload) {
  return {
    type: types.ADD_CATEGORY,
    payload
  };
}

/**
 * containers/home/action.js : ADD_CATEGORY_SUCCESS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addCategorySuccess action object
 **/
export function addCategorySuccess(payload) {
  return {
    type: types.ADD_CATEGORY_SUCCESS,
    payload
  };
}

/**
 * containers/home/action.js : ADD_CATEGORY_ERROR action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addCategoryError action object
 **/
export function addCategoryError(payload) {
  return {
    type: types.ADD_CATEGORY_ERROR,
    payload
  };
}

/**
 * containers/home/action.js : ADD_CATEGORY_CANCEL action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addCategoryCancel action object
 **/
export function addCategoryCancel(payload) {
  return {
    type: types.ADD_CATEGORY_CANCEL,
    payload
  };
}

/**
 * containers/home/action.js : ADD_POINT action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addPoint action object
 **/
export function addPoint(payload) {
  return {
    type: types.ADD_POINT,
    payload
  };
}

/**
 * containers/home/action.js : ADD_POINT_SUCCESS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addPointSuccess action object
 **/
export function addPointSuccess(payload) {
  return {
    type: types.ADD_POINT_SUCCESS,
    payload
  };
}

/**
 * containers/home/action.js : ADD_POINT_ERROR action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addPointError action object
 **/
export function addPointError(payload) {
  return {
    type: types.ADD_POINT_ERROR,
    payload
  };
}

/**
 * containers/home/action.js : ADD_POINT_CANCEL action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the addPointCancel action object
 **/
export function addPointCancel(payload) {
  return {
    type: types.ADD_POINT_CANCEL,
    payload
  };
}

/**
 * containers/home/action.js : GET_POINTS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getPoints action object
 **/
export function getPoints(payload) {
  return {
    type: types.GET_POINTS,
    payload
  };
}

/**
 * containers/home/action.js : GET_POINTS_SUCCESS action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getPointsSuccess action object
 **/
export function getPointsSuccess(payload) {
  return {
    type: types.GET_POINTS_SUCCESS,
    payload
  };
}

/**
 * containers/home/action.js : GET_POINTS_ERROR action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getPointsError action object
 **/
export function getPointsError(payload) {
  return {
    type: types.GET_POINTS_ERROR,
    payload
  };
}

/**
 * containers/home/action.js : GET_POINTS_CANCEL action factory
 * @param {any} payload the payload of the action
 * @returns {Object} the getPointsCancel action object
 **/
export function getPointsCancel(payload) {
  return {
    type: types.GET_POINTS_CANCEL,
    payload
  };
}
