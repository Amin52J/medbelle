import * as types from '@constants/actionTypes';
import * as actions from '@containers/home/action';
import Common from '@constants/common';

/**
 * containers/home/epic.js : getCategories epic
 * @param {Object} action$ stream of actions
 * @param {Object} store the main store to be used
 * @param {Object} dependencies the dependencies to be injected inside the epic
 *   @param {Function} dependencies.ajax the custom rxjs ajax object
 *   @param {Function} dependencies.of the rxjs of function
 * @returns {Array} an array of actions to be dispatched
 **/
export const getCategories = (action$, store, {ajax, of}) => action$.ofType(types.GET_CATEGORIES)
  .mergeMap(() => ajax.get(`${Common.api.base}/${Common.config.APP_ID}/${Common.config.API_KEY}${Common.api.categories}`)
    .map(response => actions.getCategoriesSuccess(response))
    .takeUntil(action$.ofType(types.GET_CATEGORIES_CANCEL))
    .catch(error => of(actions.getCategoriesError(error)))
  );

/**
 * containers/home/epic.js : addCategory epic
 * @param {Object} action$ stream of actions
 * @param {Object} store the main store to be used
 * @param {Object} dependencies the dependencies to be injected inside the epic
 *   @param {Function} dependencies.ajax the custom rxjs ajax object
 *   @param {Function} dependencies.of the rxjs of function
 * @returns {Array} an array of actions to be dispatched
 **/
export const addCategory = (action$, store, {ajax, of}) => action$.ofType(types.ADD_CATEGORY)
  .mergeMap(action => ajax.put(`${Common.api.base}/${Common.config.APP_ID}/${Common.config.API_KEY}${Common.api.categories}/${action.payload}`)
    .map(response => actions.addCategorySuccess(response))
    .takeUntil(action$.ofType(types.ADD_CATEGORY_CANCEL))
    .catch(error => of(actions.addCategoryError(error)))
  );

/**
 * containers/home/epic.js : addPoint epic
 * @param {Object} action$ stream of actions
 * @param {Object} store the main store to be used
 * @param {Object} dependencies the dependencies to be injected inside the epic
 *   @param {Function} dependencies.ajax the custom rxjs ajax object
 *   @param {Function} dependencies.of the rxjs of function
 * @returns {Array} an array of actions to be dispatched
 **/
export const addPoint = (action$, store, {ajax, of}) => action$.ofType(types.ADD_POINT)
  .mergeMap(action => ajax.post(`${Common.api.base}/${Common.config.APP_ID}/${Common.config.API_KEY}${Common.api.points}`, {...action.payload})
    .map(response => actions.addPointSuccess(response))
    .takeUntil(action$.ofType(types.ADD_POINT_CANCEL))
    .catch(error => of(actions.addPointError(error)))
  );

/**
 * containers/home/epic.js : getPoints epic
 * @param {Object} action$ stream of actions
 * @param {Object} store the main store to be used
 * @param {Object} dependencies the dependencies to be injected inside the epic
 *   @param {Function} dependencies.ajax the custom rxjs ajax object
 *   @param {Function} dependencies.of the rxjs of function
 * @returns {Array} an array of actions to be dispatched
 **/
export const getPoints = (action$, store, {ajax, of}) => action$.ofType(types.GET_POINTS)
  .mergeMap(action => ajax.get(`${Common.api.base}/${Common.config.APP_ID}/${Common.config.API_KEY}${Common.api.points}?nwlat=${action.payload.nwlat}&nwlon=${action.payload.nwlon}&selat=${action.payload.selat}&selon=${action.payload.selon}`)
    .map(response => actions.getPointsSuccess(response))
    .takeUntil(action$.ofType(types.GET_POINTS_CANCEL))
    .catch(error => of(actions.getPointsError(error)))
  );
