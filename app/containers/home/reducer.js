import * as types from '@constants/actionTypes';
import Utils from '@constants/utils';

const initialState = {
  categories: [],
  points: []
};

/**
 * containers/home/reducer.js : home reducer
 * @param {Object} state the state of the home container
 * @param {Object} action the redux action instance
 * @returns {Object} returns the new state
 **/
export default function homeState(state = initialState, action) {
  let newCategories = [];
  if (state.categories) {
    newCategories = [...state.categories];
  }
  switch (action.type) {
    case types.GET_CATEGORIES:
      return {
        ...state,
        loading: true
      };

    case types.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.payload && action.payload.response && [...action.payload.response.map((item, index) => ({
          ...item,
          color: Utils.rainbow(action.payload.response.length, index)
        }))],
        loading: false
      };

    case types.GET_CATEGORIES_ERROR:
      return {
        ...state,
        loading: false
      };

    case types.GET_CATEGORIES_CANCEL:
      return {
        ...state,
        loading: false
      };

    case types.ADD_CATEGORY:
      return {
        ...state,
        loading: true
      };

    case types.ADD_CATEGORY_SUCCESS:
      newCategories.push(action.payload.response);
      return {
        ...state,
        categories: [...(action.payload && action.payload.response &&
          newCategories.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 0))
          .map((item, index) => ({
            ...item,
            color: Utils.rainbow(newCategories.length, index)
          }))
        || state.categories],
        loading: false
      };

    case types.ADD_CATEGORY_ERROR:
      return {
        ...state,
        loading: false
      };

    case types.ADD_CATEGORY_CANCEL:
      return {
        ...state,
        loading: false
      };

    case types.ADD_POINT:
      return {
        ...state,
        loading: true
      };

    case types.ADD_POINT_SUCCESS:
      return {
        ...state,
        loading: false,
        points: [...state.points, action.payload.geopoint]
      };

    case types.ADD_POINT_ERROR:
      return {
        ...state,
        loading: false
      };

    case types.ADD_POINT_CANCEL:
      return {
        ...state,
        loading: false
      };

    case types.GET_POINTS:
      return {
        ...state,
        loading: true
      };

    case types.GET_POINTS_SUCCESS:
      return {
        ...state,
        loading: false,
        points: [...action.payload.response]
      };

    case types.GET_POINTS_ERROR:
      return {
        ...state,
        loading: false
      };

    case types.GET_POINTS_CANCEL:
      return {
        ...state,
        loading: false
      };

    default:
      return state;
  }
}
