import React from 'react';
import {shallow} from 'enzyme';
import Home from '../index';

describe('Home Container - Container', () => {
  it('renders with no problem', () => {
    const component = shallow(
      <Home/>
    );

    expect(component.find('div').first().hasClass('home-page')).toEqual(false);
  });
});
