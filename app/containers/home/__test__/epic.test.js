import React from 'react';
import * as epics from '../epic';
import * as types from '../../../constants/actionTypes';
import {ActionsObservable} from 'redux-observable';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/takeUntil';

const action$ = ActionsObservable.of({type: types.GET_CATEGORIES, payload: '/api/getCategories'});

describe('Home Container - epics', () => {
  it('successfully gets login action result', done => {
    const ajax = {
      get: () => Observable.of({})
    };
    const expectedOutputActions = [{type: types.GET_CATEGORIES_SUCCESS, payload: {}}];
    epics.getCategories(action$, null, {ajax})
      .toArray()
      .subscribe(actualOutputActions => {
          expect(actualOutputActions).toEqual(expectedOutputActions);
          done();
        }
      );
  });

  it('fails to get getCategories action result', done => {
    const ajax = {
      get: () => Observable.throw('failed')
    };
    const expectedOutputActions = [{type: types.GET_CATEGORIES_ERROR, payload: 'failed'}];
    epics.getCategories(action$, null, {ajax, of: Observable.of})
      .toArray()
      .subscribe(actualOutputActions => {
          expect(actualOutputActions).toEqual(expectedOutputActions);
          done();
        }
      );
  });
});
