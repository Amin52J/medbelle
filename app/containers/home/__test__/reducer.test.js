import React from 'react';
import reducer from '../reducer';
import * as types from '../../../constants/actionTypes';

describe('Home Container - reducers', () => {
  it('sets loading state', () => {
    expect(reducer({}, {
      type: types.GET_CATEGORIES
    })).toEqual({
      loading: true
    });
  });

  it('sets success state', () => {
    expect(reducer({}, {
      type: types.GET_CATEGORIES_SUCCESS
    })).toEqual({
      loading: false
    });
  });

  it('sets error state', () => {
    expect(reducer({}, {
      type: types.GET_CATEGORIES_ERROR
    })).toEqual({
      loading: false
    });
  });

  it('returns the initial state', () => {
    expect(reducer({
      categories: [],
      points: []
    }, {
      categories: [],
      points: []
    })).toEqual({
      categories: [],
      points: []
    });
  });
});
