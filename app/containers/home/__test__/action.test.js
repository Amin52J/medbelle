import React from 'react';
import * as actions from '../action';
import * as types from '../../../constants/actionTypes';

describe('Home Container - actions', () => {
  it('dispatches getCategories action', () => {
    expect(actions.getCategories('some text')).toEqual({
      type: types.GET_CATEGORIES,
      payload: 'some text'
    });
  });

  it('dispatches getCategoriesSuccess action', () => {
    expect(actions.getCategoriesSuccess('some text')).toEqual({
      type: types.GET_CATEGORIES_SUCCESS,
      payload: 'some text'
    });
  });

  it('dispatches getCategoriesError action', () => {
    expect(actions.getCategoriesError('some text')).toEqual({
      type: types.GET_CATEGORIES_ERROR,
      payload: 'some text'
    });
  });

  it('dispatches getCategoriesCancel action', () => {
    expect(actions.getCategoriesCancel('some text')).toEqual({
      type: types.GET_CATEGORIES_CANCEL,
      payload: 'some text'
    });
  });

  it('dispatches addCategory action', () => {
    expect(actions.addCategory('some text')).toEqual({
      type: types.ADD_CATEGORY,
      payload: 'some text'
    });
  });

  it('dispatches addCategorySuccess action', () => {
    expect(actions.addCategorySuccess('some text')).toEqual({
      type: types.ADD_CATEGORY_SUCCESS,
      payload: 'some text'
    });
  });

  it('dispatches addCategoryError action', () => {
    expect(actions.addCategoryError('some text')).toEqual({
      type: types.ADD_CATEGORY_ERROR,
      payload: 'some text'
    });
  });

  it('dispatches addCategoryCancel action', () => {
    expect(actions.addCategoryCancel('some text')).toEqual({
      type: types.ADD_CATEGORY_CANCEL,
      payload: 'some text'
    });
  });

  it('dispatches addPoint action', () => {
    expect(actions.addPoint('some text')).toEqual({
      type: types.ADD_POINT,
      payload: 'some text'
    });
  });

  it('dispatches addPointSuccess action', () => {
    expect(actions.addPointSuccess('some text')).toEqual({
      type: types.ADD_POINT_SUCCESS,
      payload: 'some text'
    });
  });

  it('dispatches addPointError action', () => {
    expect(actions.addPointError('some text')).toEqual({
      type: types.ADD_POINT_ERROR,
      payload: 'some text'
    });
  });

  it('dispatches addPointCancel action', () => {
    expect(actions.addPointCancel('some text')).toEqual({
      type: types.ADD_POINT_CANCEL,
      payload: 'some text'
    });
  });

  it('dispatches getPoints action', () => {
    expect(actions.getPoints('some text')).toEqual({
      type: types.GET_POINTS,
      payload: 'some text'
    });
  });

  it('dispatches getPointsSuccess action', () => {
    expect(actions.getPointsSuccess('some text')).toEqual({
      type: types.GET_POINTS_SUCCESS,
      payload: 'some text'
    });
  });

  it('dispatches getPointsError action', () => {
    expect(actions.getPointsError('some text')).toEqual({
      type: types.GET_POINTS_ERROR,
      payload: 'some text'
    });
  });

  it('dispatches getPointsCancel action', () => {
    expect(actions.getPointsCancel('some text')).toEqual({
      type: types.GET_POINTS_CANCEL,
      payload: 'some text'
    });
  });
});
