import React from 'react';
import ReactDOM from 'react-dom';
import * as actions from '@containers/home/action';
import Common from '@constants/common';

/* elements */
import Map from '@elements/map';
import Form from '@elements/form';
import Input from '@elements/input';
import Button from '@elements/button';
import Checkbox from '@elements/checkbox';
import Select from '@elements/select';
import Textarea from '@elements/textarea';
import DatePicker from '@elements/datepicker';
import Pin from '@elements/pin';

/* components */
import Categories from '@components/categories';

/* hoc */
import reduxConnect from '@hoc/reduxConnect';

/**
 * containers/home : Home container
 * @param {Object} props the properties of the container
 * @param {Object} props.home the redux state of the container
 * @param {Object} props.home.test the test property of the container
 **/
class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showAddPointForm: false,
      filter: null,
      lat: 0,
      lng: 0,
      categories: [],
      requiredWeather: '',
      imageUrl: '',
      name: '',
      description: '',
      recommendation_reason: '',
      timeOfDay: '',
      nextPossibleDate: '',
      groupActivity: false
    };

    this.handleAddPointClick = this.handleAddPointClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.addPointSubmit = this.addPointSubmit.bind(this);
    this.closeAddPointForm = this.closeAddPointForm.bind(this);
    this.onMapChange = this.onMapChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }

  componentWillMount() {
    this.props.getCategories();
  }

  onMapChange({bounds}) {
    this.props.getPoints({
      nwlat: bounds.nw.lat,
      nwlon: bounds.nw.lng,
      selat: bounds.se.lat,
      selon: bounds.se.lng
    });
  }

  handleAddPointClick(addPoint) {
    this.setState({
      lat: addPoint.lat,
      lng: addPoint.lng,
      showAddPointForm: true
    });
  }

  addPointSubmit(e) {
    e.preventDefault();
    this.props.addPoint({
      latitude: this.state.lat,
      longitude: this.state.lng,
      categories: this.state.categories,
      metadata: {
        requiredWeather: this.state.requiredWeather,
        imageUrl: this.state.imageUrl,
        name: this.state.name,
        description: this.state.description,
        recommendation_reason: this.state.recommendation_reason,
        timeOfDay: this.state.timeOfDay,
        nextPossibleDate: this.state.nextPossibleDate,
        groupActivity: this.state.groupActivity,
        categories: this.state.categories
      }
    });
    this.setState({
      showAddPointForm: false
    });
  }

  handleInputChange(e) {
    const newState = {...this.state};
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  }

  handleSelectChange(name, value) {
    const newState = {...this.state};
    newState[name] = value;
    this.setState(newState);
  }

  handleDateChange(name, value) {
    const newState = {...this.state};
    newState[name] = value.toDate();
    this.setState(newState);
  }

  handleCheckboxChange(e) {
    const newState = {...this.state};
    newState[e.target.name] = e.target.checked;
    this.setState(newState);
  }

  closeAddPointForm(e) {
    if (e.target !== ReactDOM.findDOMNode(this.addPointForm) &&
      !ReactDOM.findDOMNode(this.addPointForm).contains(e.target) &&
      (document.querySelector('.ant-select-dropdown') ? !document.querySelector('.ant-select-dropdown').contains(e.target) : true) &&
      (document.querySelector('.ant-calendar-picker-container') ? !document.querySelector('.ant-calendar-picker-container').contains(e.target) : true)
    ) {
      this.setState({showAddPointForm: false});
    }
  }

  onFilterChange(filter) {
    this.setState({filter: filter.selected});
  }

  render() {
    const {homeState: {categories, loading, points}} = this.props;

    return (
      <div className="home-page">
        <Map
          onChange={this.onMapChange}
          onAddPointClick={this.handleAddPointClick}
        >
          {
            points.filter(item => this.state.filter ? item.categories.indexOf(this.state.filter) > -1 : true)
              .map(item => (
                <Pin
                  onClick={() => {
                    console.log(item);
                  }}
                  lat={item.latitude}
                  lng={item.longitude}
                  color={
                    categories &&
                    categories.length > 0 &&
                    categories.filter(cat => cat.name === item.categories[0])[0] ?
                      categories.filter(cat => cat.name === item.categories[0])[0].color : null}
                  key={`pin_${item.objectId}`}
                />
              ))
          }
        </Map>
        <Categories data={categories} onAdd={this.props.addCategory} onFilterChange={this.onFilterChange}/>
        {
          this.state.showAddPointForm &&
          <div className="add-point-form" onClick={this.closeAddPointForm}>
            <Form onSubmit={this.addPointSubmit} ref={addPointForm => {
              this.addPointForm = addPointForm;
            }}>
              <h2>{Common.texts.addNewActivity}</h2>
              <img src={this.state.imageUrl} alt={this.state.name}/>
              <Input
                name="imageUrl"
                placeholder={Common.texts.imageUrl}
                onChange={this.handleInputChange}
                value={this.state.imageUrl}
              />
              <Input
                name="name"
                placeholder={Common.texts.name}
                onChange={this.handleInputChange}
                value={this.state.name}
              />
              <Textarea
                name="description"
                placeholder={Common.texts.description}
                onChange={this.handleInputChange}
                value={this.state.description}
              />
              <Select
                placeholder={Common.texts.timeOfDay}
                onChange={this.handleSelectChange.bind(this, 'timeOfDay')}
                options={Common.values.timeOfDay}
              />
              <Input
                name="requiredWeather"
                placeholder={Common.texts.requiredWeather}
                onChange={this.handleInputChange}
                value={this.state.requiredWeather}
              />
              <Textarea
                name="recommendation_reason"
                placeholder={Common.texts.recommendationReason}
                onChange={this.handleInputChange}
                value={this.state.recommendation_reason}
              />
              <DatePicker
                placeholder={Common.texts.nextPossibleDate}
                onChange={this.handleDateChange.bind(this, 'nextPossibleDate')}
              />
              <Select
                mode="multiple"
                placeholder={Common.texts.categories}
                onChange={this.handleSelectChange.bind(this, 'categories')}
                options={categories.map(item => item.name)}
              />
              <Checkbox
                name="groupActivity"
                onChange={this.handleCheckboxChange}
                checked={this.state.groupActivity}
              >
                {Common.texts.groupActivity}
              </Checkbox>
              <Button htmlType="submit" type="primary" disabled={loading}>{Common.texts.addActivity}</Button>
            </Form>
          </div>
        }
      </div>
    );
  }
}

export default reduxConnect(Home, actions);
