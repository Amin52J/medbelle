import React from 'react';
import reducer from '../reducer';

describe('App Container - reducers', () => {

  it('returns the initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });
});
