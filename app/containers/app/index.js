import React from 'react';
import * as actions from '@containers/app/action';
import {reduxConnect} from '@hoc';
import Routes from '@constants/routes';

/**
 * containers/app : App container
 * @param {Object} props the properties of the container
 * @param {Object} props.app the redux state of the container
 * @param {Object} props.app.appData the appData property of the container
 **/
class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app">
        <Routes/>
      </div>
    );
  }
}

export default reduxConnect(App, actions);
