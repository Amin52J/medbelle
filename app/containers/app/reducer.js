const initialState = {};

/**
 * containers/app/reducer.js : app reducer
 * @param {Object} state the state of the app container
 * @param {Object} action the redux action instance
 * @returns {Object} returns the new state
 **/
export default function appState(state = initialState, action) {
  switch (action.type) {

    default:
      return state;
  }
}
