// add all new epics here to combine and export
import {combineEpics} from 'redux-observable/lib/cjs/combineEpics';
import {login} from '@containers/authentication/epic';
import {getCategories, addCategory, addPoint, getPoints} from '@containers/home/epic';

export default combineEpics(
  login,
  getCategories,
  addCategory,
  addPoint,
  getPoints
);
