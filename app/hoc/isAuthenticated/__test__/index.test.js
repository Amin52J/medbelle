import React from 'react';
import isAuthenticated from '../index';
import Cookies from 'js-cookie';
import Common from '@constants/common';

describe('isAuthenticated HOC - HOC', () => {
  it('checks the authentication status', () => {
    const user = Cookies.getJSON(Common.cookie.user);
    expect(isAuthenticated()).toEqual(user ? user : undefined);
  });
});
