import Cookies from 'js-cookie';
import Common from '@constants/common';

/**
 * hoc/isAuthenticated : checks if the user is authenticated or not
 * @returns {boolean} returns true if user is authenticated or false if not
 **/
export default () => Cookies.getJSON(Common.cookie.user);
