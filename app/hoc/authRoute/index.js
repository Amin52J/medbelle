import React from 'react';
import PropTypes from 'prop-types';
import Redirect from 'react-router-dom/Redirect';
import Route from 'react-router-dom/Route';
import Common from '@constants/common';
import isAuthenticated from '@hoc/isAuthenticated';

/**
 * hoc/authRoute : AuthRoute
 * @param {object|function} component the component to be authenticated
 * @param {object} isAuth is the component the authentication component itself or not
 * @param {object} props other properties
 * @return {object} the route to the component if logged in or the route to the authentication if not
 **/
const AuthRoute = ({component, isAuth, ...props}) => {
  if (isAuthenticated()) {
    // User is Authenticated
    if (isAuth) {
      return <Redirect to={Common.routes.home}/>;
    }
    return <Route {...props} component={component}/>;
  }
  // User is not Authenticated
  if (isAuth) {
    return <Route {...props} component={component}/>;
  }
  return <Redirect to={Common.routes.authentication}/>;
};

AuthRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func
  ]),
  isAuth: PropTypes.bool
};

export default AuthRoute;
