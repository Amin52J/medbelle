// add all new HOCs here to export
export {default as reduxConnect} from '@hoc/reduxConnect';
export {default as asyncComponent} from '@hoc/asyncComponent';
export {default as AuthRoute} from '@hoc/authRoute';
export {default as isAuthenticated} from '@hoc/isAuthenticated';
