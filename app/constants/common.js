// write all common functionalities and variables here
const Common = {
  config: {
    APP_ID: '053F662C-C462-77C3-FF9E-F023F21BDA00',
    API_KEY: 'DC4E6DCD-6F9B-C38E-FF2A-17CE9DAE7800'
  },
  api: {
    base: 'https://api.backendless.com',
    login: '/users/login',
    categories: '/geo/categories',
    points: '/geo/points'
  },
  cookie: {
    user: 'MEDBELLE_USER'
  },
  routes: {
    home: '/',
    authentication: '/authentication'
  },
  texts: {
    addNewActivity: 'Add New Activity',
    addActivity: 'Add Activity',
    addNewCat: 'Add New Category',
    categories: 'Categories',
    categoryName: 'Category Name',
    description: 'Description',
    email: 'Email',
    groupActivity: 'Is Group Activity?',
    imageUrl: 'Image URL',
    login: 'Login',
    medbelle: 'Medbelle',
    name: 'Name',
    nextPossibleDate: 'Next Possible Date',
    password: 'Password',
    recommendationReason: 'Recommendation Reason',
    requiredWeather: 'Required Weather',
    timeOfDay: 'Time Of Day'
  },
  values: {
    map: {
      zoom: 15,
      key: 'AIzaSyDmgCoPxzVXUD_PRNtiwC7Le_31bZYwAHg',
      center: {
        lat: 35.763600,
        lng: 51.426297
      }
    },
    timeOfDay: ['Morning', 'Noon', 'Evening', 'Whole Day']
  }
};

export default Common;
