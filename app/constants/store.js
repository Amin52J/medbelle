import {routerReducer} from 'react-router-redux/reducer';
import routerMiddleware from 'react-router-redux/middleware';
import createStore from 'redux/lib/createStore';
import combineReducers from 'redux/lib/combineReducers';
import applyMiddleware from 'redux/lib/applyMiddleware';
import {createEpicMiddleware} from 'redux-observable/lib/cjs/createEpicMiddleware';
import createHistory from 'history/createBrowserHistory';
import isAuthenticated from '@hoc/isAuthenticated';

// rxjs observables
import {ajax} from 'rxjs/observable/dom/ajax';
import {of} from 'rxjs/observable/of';
// ----------------

import * as reducers from '@reducers';
import epics from '@epics';

// global definitions
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/takeUntil';
// ------------------

export const history = createHistory();

const customAjax = {
  post(url, data) {
    return ajax(
      {
        url,
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          'user-token': isAuthenticated() ? isAuthenticated()['user-token'] : null
        },
        crossDomain: true,
        createXHR: function () {
          return new XMLHttpRequest();
        }
      });
  },
  get(url) {
    return ajax(
      {
        url,
        method: 'GET',
        headers: {
          'user-token': isAuthenticated() ? isAuthenticated()['user-token'] : null
        },
        crossDomain: true,
        createXHR: function () {
          return new XMLHttpRequest();
        }
      });
  },
  put(url) {
    return ajax(
      {
        url,
        method: 'PUT',
        headers: {
          'user-token': isAuthenticated() ? isAuthenticated()['user-token'] : null
        },
        crossDomain: true,
        createXHR: function () {
          return new XMLHttpRequest();
        }
      });
  }
};

// the epics middleware
const epicMiddleware = createEpicMiddleware(epics, {
  dependencies: {
    ajax: customAjax,
    of
  }
});

const reducer = combineReducers({
  ...reducers,
  routing: routerReducer
});

// the reducers
const store = createStore(reducer, applyMiddleware(epicMiddleware, routerMiddleware(history)));

export default store;
