/* eslint no-undef: 0 */
import React from 'react';
import Switch from 'react-router/Switch';
import {asyncComponent} from '@hoc';
import {AuthRoute} from '@hoc';
import Common from '@constants/common';

const Home = asyncComponent(() => System.import(/* webpackChunkName: "js/chunks/home" */'@containers/home').then(module => module.default));
const Authentication = asyncComponent(() => System.import(/* webpackChunkName: "js/chunks/authentication" */'@containers/authentication').then(module => module.default));

/**
 * constants/routes.js : Routes constant
 * @returns {HTMLElement} The matched component with the URL
 **/
export default () => (
  <Switch>
    <AuthRoute
      exact
      path={Common.routes.home}
      component={Home}
    />
    <AuthRoute
      exact
      path={Common.routes.authentication}
      component={Authentication}
      isAuth
    />
  </Switch>
);
