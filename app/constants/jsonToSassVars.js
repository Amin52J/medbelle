function jsonToSassVars(obj, indent) {
  // Make object root properties into sass variables
  let sass = '';
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      sass += `$${key}:${JSON.stringify(obj[key], null, indent)}${';\n'}`;
    }
  }

  // Store string values (so they remain unaffected)
  const storedStrings = [];
  sass = sass.replace(/(["'])(?:(?=(\\?))\2.)*?\1/g, str => {
    const id = `___JTS${storedStrings.length}`;
    storedStrings.push({id: id, value: str});
    return id;
  });

  // Convert js lists and objects into sass lists and maps
  sass = sass.replace(/[{[]/g, '(').replace(/[}\]]/g, ')');

  // Put string values back (now that we're done converting)
  storedStrings.forEach(str => {
    sass = sass.replace(str.id, str.value.split('"').join(''));
  });

  return sass;
}

module.exports = jsonToSassVars;
