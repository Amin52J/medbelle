import React from 'react';
import ReactDOM from 'react-dom';
import Provider from 'react-redux/lib/components/Provider';
import store, {history} from '@constants/store';
import ConnectedRouter from 'react-router-redux/ConnectedRouter';
import App from '@containers/app/index';

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
);
