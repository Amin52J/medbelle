// add all new reducers here to export
export {default as homeState} from '@containers/home/reducer';
export {default as appState} from '@containers/app/reducer';
export {default as authenticationState} from '@containers/authentication/reducer';
