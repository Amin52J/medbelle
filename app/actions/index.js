// add actions of the newly created container here.
import * as home from '@containers/home/action';
import * as app from '@containers/app/action';
import * as authentication from '@containers/authentication/action';

export default {home, app, authentication}; // exports all the actions.
