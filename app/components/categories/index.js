import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Common from '@constants/common';

/* elements */
import Button from '@elements/button';
import Input from '@elements/input';

/**
 * components/categories : Categories component
 * @param {object} props the properties of the component
 * @param {array} props.data the categories data
 * @param {function} props.onFilterChange the filter change callback
 * @returns {HTMLElement} returns categories component's node
 **/
class Categories extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null,
      addCatVisible: false,
      newCatVal: ''
    };

    this.selected = this.selected.bind(this);
    this.getColor = this.getColor.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.cancel = this.cancel.bind(this);
    this.changeAddCatVisibility = this.changeAddCatVisibility.bind(this);
    this.handleAddCatInvisibilityClick = this.handleAddCatInvisibilityClick.bind(this);
    this.handleAddCatInput = this.handleAddCatInput.bind(this);
    this.addNewCat = this.addNewCat.bind(this);
  }

  componentDidMount() {
    document.body.addEventListener('click', this.handleAddCatInvisibilityClick);
  }

  componentWillUnmount() {
    document.body.removeEventListener('click', this.handleAddCatInvisibilityClick);
  }

  /**
   * components/categories : Categories component - the category or sub-category select action callback
   * @param {number} id the id of the newly selected category or sub-category
   * @returns {void}
   **/
  selected(id) {
    if (this.state.selected !== id) {
      this.setState({selected: id});
      this.props.onFilterChange({
        selected: id
      });
    }
    else {
      this.setState({selected: null});
      this.props.onFilterChange({
        selected: null
      });
    }
  }

  /**
   * components/categories : Categories component - gets the color based on the state
   * @param {number} color the color of the category
   * @returns {number} the color color of the selected category or sub-category
   **/
  getColor(color) {
    return color;
  }

  /**
   * components/categories : Categories component - gets the style of the category or sub-category based on the state
   * @param {number} color the color of the category
   * @param {boolean} isSelected whether or not the target sub-category is selected
   * @returns {object} the style object based on the state
   **/
  getStyle(color, isSelected) {
    return {
      color: isSelected ? 'inherit' : this.getColor(color),
      borderColor: this.getColor(color),
      backgroundColor: isSelected ? this.getColor(color) : 'inherit'
    };
  }

  /**
   * components/categories : Categories component - cancels (resets) all the filtering actions
   * @returns {void}
   **/
  cancel() {
    this.setState({
      selected: null
    });
    this.props.onFilterChange({
      selected: null
    });
  }

  changeAddCatVisibility() {
    if (!this.state.addCatVisible) {
      ReactDOM.findDOMNode(this.addCat).focus();
    }
    this.setState({addCatVisible: !this.state.addCatVisible});
  }

  handleAddCatInvisibilityClick(e) {
    if (e.target !== ReactDOM.findDOMNode(this.addCat) && !ReactDOM.findDOMNode(this.addCat).contains(e.target)) {
      this.setState({addCatVisible: false});
    }
  }

  handleAddCatInput(e) {
    this.setState({newCatVal: e.target.value});
  }

  addNewCat(e) {
    if (e.key === 'Enter') {
      this.props.onAdd(this.state.newCatVal);
      this.setState({newCatVal: '', addCatVisible: false});
    }
  }

  render() {
    return (
      <div className={`categories-component ${this.props.className || ''}`}>
        <div className="add-category">
          <Button title={Common.texts.addNewCat} onClick={this.changeAddCatVisibility}>+</Button>
          <Input
            ref={addCat => {
              this.addCat = addCat;
            }}
            className={this.state.addCatVisible ? 'visible' : ''}
            placeholder={Common.texts.categoryName}
            onChange={this.handleAddCatInput}
            value={this.state.newCatVal}
            onKeyPress={this.addNewCat}
          />
        </div>
        <div className="categories-content">
          {
            this.props.data.map(category => (
              <p
                key={`category_${category.objectId}`}
                className="category"
                onClick={this.selected.bind(this, category.name)}
                style={this.getStyle(category.color, this.state.selected === category.name)}
              >
                {category.name}
              </p>
            ))
          }
        </div>
      </div>
    );
  }
}

Categories.defaultProps = {
  data: [],
  onFilterChange: () => {
  },
  onAdd: () => {
  }
};

Categories.propTypes = {
  data: PropTypes.array,
  onFilterChange: PropTypes.func,
  onAdd: PropTypes.func
};

export default Categories;
