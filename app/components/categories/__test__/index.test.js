import React from 'react';
import {mount} from 'enzyme';
import Categories from '../index';
import renderer from 'react-test-renderer';

describe('Categories Component', () => {
  it('renders with no problem', () => {
    const component = mount(
      <Categories/>
    );
    const tree = renderer.create(component).toJSON();

    expect(component.find('div').first().hasClass('categories-component')).toEqual(true);
    expect(tree).toMatchSnapshot();
  });
});
